# frozen_string_literal: true

VERSION = '43.0.1' unless defined?(::VERSION)
